import { TinyMCEContext } from "./contexts";
import { QuickInsert } from "./core";

export function registerTinyMCEPlugin(): void {
  // TinyMCE addon registration
  tinymce.PluginManager.add("quickinsert", function (editor) {
    editor.on("keydown", evt => {
      if (QuickInsert.matchBoundKeyEvent(evt)) {
        evt.stopPropagation();
        evt.preventDefault();
        const context = new TinyMCEContext(editor);
        context.startText = editor.selection.getContent().trim();
        QuickInsert.open(context);
      }
    });
    editor.ui.registry.addButton("quickinsert", {
      tooltip: "Quick Insert",
      icon: "search",
      onAction: function () {
        if (QuickInsert.app.embeddedMode) return;
        // Open window
        QuickInsert.open(new TinyMCEContext(editor));
      },
    });
  });
  CONFIG.TinyMCE.plugins = CONFIG.TinyMCE.plugins + " quickinsert";
  CONFIG.TinyMCE.toolbar = CONFIG.TinyMCE.toolbar + " quickinsert";
}
