const MODULE_NAME = "quick-insert";
export const SAVE_SETTINGS_REVISION = 1;

export enum settings {
  QUICKOPEN = "quickOpen",
  SEARCH_ANYWHERE_COMPAT = "searchAnywhereCompatible",
  LIST_STYLE = "listStyle",
  ENABLE_GLOBAL_CONTEXT = "enableGlobalContext",
  INDEXING_DISABLED = "indexingDisabled",
  FILTERS_CLIENT = "filtersClient",
  FILTERS_WORLD = "filtersWorld",
  FILTERS_SHEETS = "filtersSheets",
  FILTERS_SHEETS_ENABLED = "filtersSheetsEnabled",
  GM_ONLY = "gmOnly",
  INDEX_GUARD_ENABLED = "indexGuardEnabled",
  INDEX_DEFERRED_DELAY = "indexDeferredDelay",
}

type Callback = (value) => void;

const noop = (): void => {
  return;
};

const moduleSettings = [
  {
    setting: settings.GM_ONLY,
    name: "QUICKINSERT.SettingsGmOnly",
    hint: "QUICKINSERT.SettingsGmOnlyHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
  {
    setting: settings.FILTERS_SHEETS_ENABLED,
    name: "QUICKINSERT.SettingsFiltersSheetsEnabled",
    hint: "QUICKINSERT.SettingsFiltersSheetsEnabledHint",
    type: Boolean,
    default: true,
    scope: "world",
  },
  {
    setting: settings.INDEX_GUARD_ENABLED,
    name: "QUICKINSERT.SettingsIndexGuardEnabled",
    hint: "QUICKINSERT.SettingsIndexGuardEnabledHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
  // {
  //   setting: settings.INDEX_DEFERRED_DELAY,
  //   name: "QUICKINSERT.SettingsIndexDeferredDelay",
  //   hint: "QUICKINSERT.SettingsIndexDeferredDelayHint",
  //   type: Number,
  //   choices: {
  //     0: "QUICKINSERT.SettingsIndexDeferredDelayImmediate",
  //     500: "QUICKINSERT.SettingsIndexDeferredDelay05s",
  //     1000: "QUICKINSERT.SettingsIndexDeferredDelay1s",
  //     2000: "QUICKINSERT.SettingsIndexDeferredDelay2s",
  //     "-1": "QUICKINSERT.SettingsIndexDeferredDelayOnFirstOpen",
  //   },
  //   default: 1000,
  //   scope: "world",
  // },
  {
    setting: settings.QUICKOPEN,
    name: "QUICKINSERT.SettingsQuickOpen",
    hint: "QUICKINSERT.SettingsQuickOpenHint",
    type: window.Azzu.SettingsTypes.KeyBinding,
    default: "Ctrl +  ",
  },
  {
    setting: settings.ENABLE_GLOBAL_CONTEXT,
    name: "QUICKINSERT.SettingsEnableGlobalContext",
    hint: "QUICKINSERT.SettingsEnableGlobalContextHint",
    type: Boolean,
    default: true,
  },
  {
    setting: settings.INDEXING_DISABLED,
    name: "Things that have indexing disabled",
    type: Object,
    default: {
      entities: {
        Macro: [1, 2],
        Scene: [1, 2],
        Playlist: [1, 2],
        RollTable: [1, 2],
      },
      packs: {},
    },
    scope: "world",
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.FILTERS_CLIENT,
    name: "Own filters",
    type: Object,
    default: {
      saveRev: SAVE_SETTINGS_REVISION,
      disabled: [],
      filters: [],
    },
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.FILTERS_WORLD,
    name: "World filters",
    type: Object,
    default: {
      saveRev: SAVE_SETTINGS_REVISION,
      filters: [],
    },
    scope: "world",
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.FILTERS_SHEETS,
    name: "Sheet filters",
    type: Object,
    default: {},
    scope: "world",
    config: false, // Doesn't show up in config
  },
];

function registerSetting(callback: Callback, { setting, ...options }): void {
  game.settings.register(MODULE_NAME, setting, {
    config: true,
    scope: "client",
    ...options,
    onChange: callback || noop,
  });
}

export function registerSettings(
  callbacks: {
    [setting: string]: Callback;
  } = {}
): void {
  moduleSettings.forEach(item => {
    registerSetting(callbacks[item.setting], item);
  });
}

export function getSetting(setting: settings | string): any {
  return game.settings.get(MODULE_NAME, setting as string);
}

export function setSetting(setting: settings | string, value): Promise<any> {
  return game.settings.set(MODULE_NAME, setting as string, value);
}

export function registerMenu({ menu, ...options }): void {
  game.settings.registerMenu(MODULE_NAME, menu, options);
}
