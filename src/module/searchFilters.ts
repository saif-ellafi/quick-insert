import {
  enabledEntityTypes,
  EntityCollections,
  isEntity,
  isCompendiumEntity,
  SearchResult,
  packEnabled,
} from "./searchLib";
import { setSetting, settings, getSetting } from "./settings";

export interface SearchFilterConfig {
  compendiums: string[] | "any";
  folders: string[] | "any";
  entities: string[] | "any";
}

export enum FilterType {
  Default,
  World,
  Client,
}

interface WorldSavedFilters {
  filters: SearchFilter[];
}

interface ClientSavedFilters {
  disabled: string[];
  filters: SearchFilter[];
}

export interface SearchFilter {
  id: string;
  type: FilterType;
  tag: string; // used with @tag
  subTitle: string; // Title
  filterConfig?: SearchFilterConfig;
  disabled?: boolean;
}

export class SearchFilterCollection {
  disabled: string[] = [];
  dirty = true;

  defaultFilters: SearchFilter[] = [];
  clientFilters: SearchFilter[] = [];
  worldFilters: SearchFilter[] = [];
  combinedFilters: SearchFilter[] = [];

  public get filters(): SearchFilter[] {
    if (this.dirty) {
      this.combinedFilters = [
        ...this.defaultFilters,
        ...this.worldFilters,
        ...this.clientFilters,
      ];
      this.combinedFilters.forEach(
        f => (f.disabled = this.disabled.includes(f.id))
      );
      this.dirty = false;
    }
    return this.combinedFilters;
  }

  // Someone changed the filters, will be saved etc.
  filtersChanged(which?: FilterType): void {
    if (which === FilterType.Client) {
      this.saveClient();
    } else if (which === FilterType.World) {
      this.saveWorld();
    } else {
      this.save();
    }
  }

  search(query?: string): SearchFilter[] {
    if (!query) {
      return [...this.filters];
    }
    return this.filters.filter(f => f.tag.includes(query));
  }

  getFilter(id: string): SearchFilter {
    return this.filters.find(f => f.id == id);
  }

  getFilterByTag(tag: string): SearchFilter {
    return this.filters.filter(f => !f.disabled).find(f => f.tag == tag);
  }

  addFilter(filter: SearchFilter): void {
    if (filter.type == FilterType.World) {
      this.worldFilters.push(filter);
      this.filtersChanged(filter.type);
    } else if (filter.type == FilterType.Client) {
      this.clientFilters.push(filter);
      this.filtersChanged(filter.type);
    }
  }

  deleteFilter(id: string): void {
    const f = this.filters.find(f => f.id === id);
    if (f.type == FilterType.World) {
      const x = this.worldFilters.findIndex(f => f.id === id);
      if (x != -1) {
        this.worldFilters.splice(x, 1);
      }
    } else if (f.type == FilterType.Client) {
      const x = this.clientFilters.findIndex(f => f.id === id);
      if (x != -1) {
        this.clientFilters.splice(x, 1);
      }
    }
    this.filtersChanged(f.type);
  }

  resetFilters(): void {
    this.defaultFilters = [];
    this.clientFilters = [];
    this.worldFilters = [];
    this.combinedFilters = [];
    this.dirty = false;
  }

  loadDefaultFilters(): void {
    this.loadCompendiumFilters();
    // this.loadDirectoryFilters();
    this.loadEntityFilters();
    this.dirty = true;
  }

  loadEntityFilters(): void {
    this.defaultFilters = this.defaultFilters.concat(
      enabledEntityTypes().map(type => {
        return {
          id: EntityCollections[type],
          type: FilterType.Default,
          tag: EntityCollections[type],
          subTitle: `${game.i18n.localize(
            `ENTITY.${game[EntityCollections[type]].entity}`
          )}`,
          filterConfig: {
            folders: "any",
            compendiums: "any",
            entities: [game[EntityCollections[type]].entity],
          },
        };
      })
    );
  }

  loadDirectoryFilters(): void {
    // TODO: find a way to find directories that the user is allowed to see
    if (!game.user.isGM) return;

    this.defaultFilters = this.defaultFilters.concat(
      enabledEntityTypes().map(type => {
        return {
          id: `dir.${EntityCollections[type]}`,
          type: FilterType.Default,
          tag: `dir.${EntityCollections[type]}`,
          subTitle: game[EntityCollections[type]].directory?.title,
          filterConfig: {
            folders: "any",
            compendiums: [],
            entities: [game[EntityCollections[type]].entity],
          },
        };
      })
    );
  }

  loadCompendiumFilters(): void {
    this.defaultFilters = this.defaultFilters.concat(
      game.packs
        .map(pack => {
          if (!packEnabled(pack)) return null;
          return {
            id: pack.collection,
            type: FilterType.Default,
            tag: pack.collection,
            subTitle: pack.metadata.label,
            filterConfig: {
              folders: [],
              compendiums: [pack.collection],
              entities: "any",
            },
          };
        })
        .filter(f => f != null)
    );
  }

  loadClientSave(): void {
    const clientSave: ClientSavedFilters = getSetting(settings.FILTERS_CLIENT);
    this.disabled = clientSave.disabled || [];
    this.clientFilters = clientSave.filters || [];
    this.dirty = true;
  }

  loadWorldSave(): void {
    const worldSave: WorldSavedFilters = getSetting(settings.FILTERS_WORLD);
    this.worldFilters = worldSave.filters || [];
    this.dirty = true;
  }

  loadSave(): void {
    this.loadClientSave();
    this.loadWorldSave();
    Hooks.call("QuickInsert:FiltersUpdated");
  }

  saveWorld(): void {
    if (!game.user.isGM) return;

    const worldSave: WorldSavedFilters = {
      filters: [],
    };

    for (const filter of this.worldFilters) {
      delete filter.disabled;
      worldSave.filters.push(filter);
    }

    setSetting(settings.FILTERS_WORLD, worldSave);
  }

  saveClient(): void {
    const clientSave: ClientSavedFilters = {
      disabled: [],
      filters: [],
    };

    for (const filter of [
      ...this.defaultFilters,
      ...this.worldFilters,
      ...this.clientFilters,
    ]) {
      if (filter.disabled) {
        clientSave.disabled.push(filter.id);
      }
      if (filter.type === FilterType.Client) {
        clientSave.filters.push(filter);
      }
    }
    setSetting(settings.FILTERS_CLIENT, clientSave);
  }

  save(): void {
    this.saveClient();
    this.saveWorld();
  }
}

// Is parentFolder inside targetFolder?
function isInFolder(parentFolder: string, targetFolder: string): boolean {
  while (parentFolder) {
    if (parentFolder === targetFolder) return true;
    parentFolder = (game.folders.get(parentFolder)?.data as any).parent;
  }
  return false;
}

export function matchFilterConfig(
  config: SearchFilterConfig,
  item: SearchResult
): boolean {
  let folderMatch = false;
  let compendiumMatch = false;
  let entityMatch = true;

  if (isEntity(item.item)) {
    if (config.folders === "any") {
      folderMatch = true;
    } else {
      for (const f of config.folders) {
        if (isInFolder(item.item.folder?.id, f)) {
          folderMatch = true;
          break;
        }
      }
    }
  } else if (isCompendiumEntity(item.item)) {
    if (config.compendiums == "any") {
      compendiumMatch = true;
    } else {
      compendiumMatch = config.compendiums.includes(item.item.package);
    }
  }

  if (config.entities == "any") {
    entityMatch = true;
  } else {
    entityMatch = config.entities.includes(item.item.entityType);
  }

  return (folderMatch || compendiumMatch) && entityMatch;
}
