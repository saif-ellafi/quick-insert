// Extra types not covered by foundry-pc-types
interface Window {
  Azzu: {
    SettingsTypes: {
      KeyBinding: {
        parse: (
          string
        ) => {
          altKey: boolean;
          ctrlKey: boolean;
          key: string;
          location: number;
          metaKey: boolean;
          shiftKey: boolean;
        };
        eventIsForBinding: (KeyboardEvent, string) => boolean;
      };
    };
  };
}

// TODO: maybe include tinymce types via npm
interface TinyMce {
  contentAreaContainer: HTMLElement;
  insertContent(string);
  PluginManager: {
    add(arg0: string, arg1: (editor) => void);
  };
  selection: {
    getBoundingClientRect(): DOMRect;
  };
  focus(): void;
}

declare let tinymce: TinyMce;

declare let fromUuid: (uuid: string) => Promise<Entity | null>;

// The items in compendium search indexes
interface CompendiumIndexItem {
  id: string; // pre-5.5 compat
  _id: string;
  img: string;
  name: string;
}
