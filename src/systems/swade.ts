// Savage Worlds Adventure Edition integration
import { CharacterSheetContext } from "../module/contexts";
import { QuickInsert } from "../module/core";
import { getSetting, setSetting, settings } from "../module/settings";

export const SYSTEM_NAME = "swade";

export const defaultSheetFilters = {
  skill: "swade.skills",
  hindrance: "swade.hindrances",
  edge: "swade.edges",
  power: "",
  weapon: "",
  armor: "",
  shield: "",
  gear: "",
  "character.choice": "",
  "vehicle.choice": "",
  mod: "",
  "vehicle-weapon": "",
};

export class SwadeSheetContext extends CharacterSheetContext {
  entitySheet: BaseEntitySheet;
  button: JQuery<HTMLElement>;
  constructor(
    entitySheet: BaseEntitySheet,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(entitySheet, anchor);
    if (sheetType && insertType) {
      const sheetFilters = getSetting(settings.FILTERS_SHEETS).baseFilters;
      this.filter =
        sheetFilters[`${sheetType}.${insertType}`] || sheetFilters[insertType];
    }
  }
}

export function sheetSwadeRenderHook(
  app: BaseEntitySheet,
  sheetType?: string
): void {
  if (
    (app.element as JQuery<HTMLElement>).find(".quick-insert-link").length > 0
  ) {
    return;
  }
  const link = `<a class="quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  (app.element as JQuery<HTMLElement>).find("a.item-create").each((i, el) => {
    const type = el.dataset.type;
    if (!Object.keys(defaultSheetFilters).includes(type)) return;
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new SwadeSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  if (game.user.isGM) {
    const customFilters = getSetting(settings.FILTERS_SHEETS).baseFilters;
    setSetting(settings.FILTERS_SHEETS, {
      baseFilters: {
        ...defaultSheetFilters,
        ...customFilters,
      },
    });
  }
  Hooks.on(
    "renderSwadeCharacterSheet",
    app =>
      getSetting(settings.FILTERS_SHEETS_ENABLED) &&
      sheetSwadeRenderHook(app, "character")
  );
  Hooks.on("renderSwadeNPCSheet", app => sheetSwadeRenderHook(app, "npc"));
  Hooks.on("renderSwadeVehicleSheet", app =>
    sheetSwadeRenderHook(app, "vehicle")
  );

  console.log("Quick Insert | swade system extensions initiated");
}
