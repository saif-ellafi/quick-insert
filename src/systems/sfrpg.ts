// Starfinder integration
import { CharacterSheetContext } from "../module/contexts";
import { QuickInsert } from "../module/core";
import { getSetting, setSetting, settings } from "../module/settings";

export const SYSTEM_NAME = "sfrpg";

export const defaultSheetFilters = {
  class: "sfrpg.classes",
  race: "sfrpg.races",
  theme: "sfrpg.themes",
  // asi: "",
  archetype: "sfrpg.archetypes",
  feat: "sfrpg.feats",

  spell: "sfrpg.spells",

  weapon: "sfrpg.equipment",
  shield: "sfrpg.equipment",
  equipment: "sfrpg.equipment",
  consumable: "sfrpg.equipment",
  goods: "sfrpg.equipment",
  container: "sfrpg.equipment",
  technological: "sfrpg.equipment",
  fusion: "sfrpg.equipment",
  upgrade: "sfrpg.equipment",
  augmentation: "sfrpg.equipment",
};

export class SfrpgSheetContext extends CharacterSheetContext {
  entitySheet: BaseEntitySheet;
  button: JQuery<HTMLElement>;
  constructor(
    entitySheet: BaseEntitySheet,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(entitySheet, anchor);
    if (sheetType && insertType) {
      const sheetFilters = getSetting(settings.FILTERS_SHEETS).baseFilters;
      this.filter =
        sheetFilters[`${sheetType}.${insertType}`] || sheetFilters[insertType];
    }
  }
}

export function sheetSfrpgRenderHook(
  app: BaseEntitySheet,
  sheetType?: string
): void {
  if (
    (app.element as JQuery<HTMLElement>).find(".quick-insert-link").length > 0
  ) {
    return;
  }
  const link = `<a class="item-control quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  (app.element as JQuery<HTMLElement>)
    .find("a.item-create, .item-control.spell-browse")
    .each((i, el) => {
      const linkEl = $(link);
      $(el).after(linkEl);
      const type = el.dataset.type;
      linkEl.on("click", () => {
        const context = new SfrpgSheetContext(app, linkEl, sheetType, type);
        QuickInsert.open(context);
      });
    });
}

export function init(): void {
  if (game.user.isGM) {
    const customFilters = getSetting(settings.FILTERS_SHEETS).baseFilters;
    setSetting(settings.FILTERS_SHEETS, {
      baseFilters: {
        ...defaultSheetFilters,
        ...customFilters,
      },
    });
  }
  Hooks.on(
    "renderActorSheetSFRPGCharacter",
    app =>
      getSetting(settings.FILTERS_SHEETS_ENABLED) &&
      sheetSfrpgRenderHook(app, "character")
  );

  console.log("Quick Insert | sfrpg system extensions initiated");
}
